# k8s Transmission

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name        |    Description                 | Default                    |
| :---------------------- | ------------------------------ | -------------------------- |
|  `BLOCKLIST_URL`        | Transmission Blocklist URL     | undef                      |
|  `PEER_PORT`            | Transmission Peer Port         | `51413`                    |
|  `RPC_PASS`             | Transmission RPC Password      | undef                      |
|  `RPC_PORT`             | Transmission RPC Port          | `9091`                     |
|  `RPC_USER`             | Transmission RPC Username      | undef                      |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point         | Description                 |
| :-------------------------- | --------------------------- |
|  `/config`                  | Transmission Configuration  |
|  `/var/lib/transmission`    | Transmission Downloads root |
|  `/watch`                   | Transmission Torrent Queue  |

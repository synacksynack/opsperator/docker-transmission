#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh

BLOCKLIST_ENABLED=false
PEER_PORT=${PEER_PORT:-51413}
RPC_PORT=${RPC_PORT:-9091}
if test "$BLOCKLIST_URL"; then
    BLOCKLIST_ENABLED=true
else
    BLOCKLIST_URL=http://www.example.com/blocklist
fi

myshutdown()
{
    if test "$RPC_USER" -a "$RPC_PORT"; then
	/usr/bin/transmission-remote -n "$USER":"$PASS" --exit
    else
	/usr/bin/transmission-remote --exit
    fi
}

if ! test -s /config/settings.json; then
    echo Initializing Transmission Configuration
    sed \
	-e "s|BLOCKLIST_ENABLED|$BLOCKLIST_ENABLED|" \
	-e "s|BLOCKLIST_URL|$BLOCKLIST_URL|" \
	-e "s|PEER_PORT|$PEER_PORT|" \
	-e "s|RPC_PORT|$RPC_PORT|" \
	/settings.json >/config/settings.json
fi

echo Resetting Transmission RPC Configuration
if test "$RPC_USER" -a "$RPC_PASS"; then
    sed -i \
	-e '/rpc-authentication-required/c\    "rpc-authentication-required": true,' \
	-e "/rpc-username/c\    \"rpc-username\": \"$RPC_USER\"," \
	-e "/rpc-password/c\    \"rpc-password\": \"$RPC_PASS\"," \
	/config/settings.json
else
    sed -i \
	-e '/rpc-authentication-required/c\    "rpc-authentication-required": false,' \
	/config/settings.json
fi

mkdir -p /var/lib/transmission/complete /var/lib/transmission/incomplete

trap myshutdown HUP
trap myshutdown INT
trap myshutdown QUIT
trap myshutdown KILL
trap myshutdown TERM

unset RPC_USER RPC_PASS PEER_PORT RPC_PORT BLOCKLIST_ENABLED BLOCKLIST_URL

exec "$@"

#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/transmission-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to debian-transmission
	sed "s|^debian-transmission:.*|debian-transmission:x:`id -g`:|" /etc/group >/tmp/transmission-group
	sed \
	    "s|^debian-transmission:.*|debian-transmission:x:`id -u`:`id -g`:debian-transmission:/var/lib/transmission:/bin/sh|" \
	    /etc/passwd >/tmp/transmission-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/transmission-passwd
    export NSS_WRAPPER_GROUP=/tmp/transmission-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi

FROM docker.io/debian:buster-slim

# Transmission image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="Transmission." \
      io.k8s.display-name="Transmission" \
      io.openshift.expose-services="9091:http,51413:torrent" \
      io.openshift.tags="transmission,torrent" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-transmission" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="2.94"

COPY config/* /

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install Transmission" \
    && apt-get -y install ca-certificates curl findutils openssl p7zip tar \
	python3 rsync transmission-cli transmission-daemon unrar-free unzip \
	libnss-wrapper \
    && mv /nsswrapper.sh /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && for dir in /var/lib/transmission /config /watch; \
	do \
	    mkdir -p "$dir" \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning Up" \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT [ "dumb-init", "--", "/run-transmission.sh" ]
CMD /usr/bin/transmission-daemon -g /config -c /watch -f
